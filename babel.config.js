module.exports = {
    presets: ['@babel/typescript', ['@babel/env', { loose: true }], '@babel/react'],
};
