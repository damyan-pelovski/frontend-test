import React from 'react';
import { MockedProvider } from '@apollo/react-testing';
import { renderHook } from '@testing-library/react-hooks';
import renderer from 'react-test-renderer';
import { GET_CONTACTS, VIEW_CONTACT } from './queries/contacts';
import ErrorPage from './components/ErrorPage';
import ContactsList from './components/ContactsList';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import ContactDetails from './components/ContactDetails';
import ContactForm from './components/ContactForm';
import { dateFormatter } from './utils/utils';
import { useErrorHandling } from './hooks/custom';
import { ContactErrorMessage } from './models/contact.model';

const wait = async (ms = 0) => {
    await renderer.act(() => {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        });
    });
};

const getContactsQuery = [
    {
        request: {
            query: GET_CONTACTS,
        },
        result: {
            contacts: [
                {
                    id: 'test123',
                    name: 'test',
                    email: 'test@test.test',
                },
            ],
        },
    },
];

const singleContactDetailQuery = [
    {
        request: {
            query: VIEW_CONTACT,
            variables: {
                id: 'test123',
            },
        },
        result: {
            contact: {
                id: 'test123',
                name: 'test',
                email: 'test@test.test',
            },
        },
    },
];

describe('Util functions', () => {
    test('dateFormatter function should return correct result with a minute different then 1', () => {
        expect(dateFormatter('2019-11-24T12:59:17.905Z')).toBe('24-11-2019 14:59');
    });

    test('dateFormatter function should return correct result with a minute equal to 1', () => {
        expect(dateFormatter('2019-11-24T13:01:17.505Z')).toBe('24-11-2019 15:01');
    });
});

describe('Custom hooks', () => {
    test('useErrorHandling name should be empty string at start', () => {
        const { result } = renderHook(() => useErrorHandling());

        expect(result.current[0].nameErrorMessage).toBe('');
    });

    test('useErrorHandling email should be empty string at start', () => {
        const { result } = renderHook(() => useErrorHandling());

        expect(result.current[0].emailErrorMessage).toBe('');
    });

    test('useErrorHandling setting name should work correctly', () => {
        const { result } = renderHook(() => useErrorHandling());

        renderer.act(() => {
            result.current[1](ContactErrorMessage.Name, 'test error message');
        });

        expect(result.current[0].nameErrorMessage).toBe('test error message');
    });

    test('useErrorHandling setting email should work correctly', () => {
        const { result } = renderHook(() => useErrorHandling());

        renderer.act(() => {
            result.current[1](ContactErrorMessage.Email, 'test email message');
        });

        expect(result.current[0].emailErrorMessage).toBe('test email message');
    });

    test('useErrorHandling setting name and email should work correctly', () => {
        const { result } = renderHook(() => useErrorHandling());

        renderer.act(() => {
            result.current[1](ContactErrorMessage.Name, 'test name message');
            result.current[1](ContactErrorMessage.Email, 'test email message');
        });

        expect(result.current[0].nameErrorMessage).toBe('test name message');
        expect(result.current[0].emailErrorMessage).toBe('test email message');
    });

    test('useErrorHandling setting name and email and then deleting them should work correctly', () => {
        const { result } = renderHook(() => useErrorHandling());

        renderer.act(() => {
            result.current[1](ContactErrorMessage.Name, 'test name message');
            result.current[1](ContactErrorMessage.Email, 'test email message');
            result.current[2]();
        });

        expect(result.current[0].nameErrorMessage).toBe('');
        expect(result.current[0].emailErrorMessage).toBe('');
    });

    test('useErrorHandling setting name cleaning errors and then setting again should work correctly', () => {
        const { result } = renderHook(() => useErrorHandling());

        renderer.act(() => {
            result.current[1](ContactErrorMessage.Name, 'test name message');
            result.current[2]();
            result.current[1](ContactErrorMessage.Name, 'test name message');
        });

        expect(result.current[0].nameErrorMessage).toBe('test name message');
    });
});

describe('Components load with provided mocks', () => {
    test('Error page should load correctly', () => {
        const tree = renderer.create(<ErrorPage />).toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('Main App should render correctly with provided info', async () => {
        const tree = renderer
            .create(
                <MockedProvider mocks={getContactsQuery} addTypename={false}>
                    <App />
                </MockedProvider>
            )
            .toJSON();

        await wait(0);
        expect(tree).toMatchSnapshot();
    });

    test('Contact details with no mocked data should render correctly', async () => {
        const tree = renderer
            .create(
                <BrowserRouter>
                    <MockedProvider mocks={[]} addTypename={false}>
                        <ContactDetails />
                    </MockedProvider>
                </BrowserRouter>
            )
            .toJSON();

        await wait(0);
        expect(tree).toMatchSnapshot();
    });

    test('Contact details with mocked data should render correctly', async () => {
        const tree = renderer
            .create(
                <BrowserRouter>
                    <MockedProvider mocks={singleContactDetailQuery} addTypename={false}>
                        <ContactDetails />
                    </MockedProvider>
                </BrowserRouter>
            )
            .toJSON();

        await wait(0);
        expect(tree).toMatchSnapshot();
    });
    test('Contacts list with empty mocked data should render correctly', async () => {
        const tree = renderer
            .create(
                <BrowserRouter>
                    <MockedProvider mocks={[]} addTypename={false}>
                        <ContactsList contacts={[]} removeContact={() => {}} editContact={() => {}} />
                    </MockedProvider>
                </BrowserRouter>
            )
            .toJSON();

        await wait(0);
        expect(tree).toMatchSnapshot();
    });
    test('Contacts list with mocked test data should render correctly', async () => {
        const tree = renderer
            .create(
                <BrowserRouter>
                    <MockedProvider mocks={getContactsQuery} addTypename={false}>
                        <ContactsList
                            contacts={[
                                {
                                    id: 'test123',
                                    name: 'test',
                                    email: 'test@test.test',
                                },
                            ]}
                            removeContact={() => {}}
                            editContact={() => {}}
                        />
                    </MockedProvider>
                </BrowserRouter>
            )
            .toJSON();

        await wait(0);
        expect(tree).toMatchSnapshot();
    });

    test('Contact form with empty inputs should load correctly', () => {
        const tree = renderer
            .create(
                <BrowserRouter>
                    <ContactForm
                        handleSubmit={() => {}}
                        inputs={{ email: '', name: '' }}
                        nameErrorMessage={''}
                        emailErrorMessage={''}
                        handleInputChange={() => {}}
                        isEditMode={false}
                    />
                </BrowserRouter>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

describe('Loading states - Components', () => {
    test('Main App should show loadind... initiallity', async () => {
        const component = renderer.create(
            <MockedProvider mocks={[]}>
                <App />
            </MockedProvider>
        );

        const tree = component.toJSON();
        await wait(0);
        expect(tree.children).toContain('loading...');
    });

    test('Contact Details should show loadind... initiallity', async () => {
        const component = renderer.create(
            <MockedProvider mocks={[]}>
                <BrowserRouter>
                    <ContactDetails />
                </BrowserRouter>
            </MockedProvider>
        );

        const tree = component.toJSON();
        await wait(0);

        expect(tree.children).toContain('loading...');
    });
});

describe('Error handling', () => {
    test('component should show error with wrong query provided', async () => {
        const getUser = {
            request: {
                query: VIEW_CONTACT,
            },
        };

        const component = renderer.create(
            <MockedProvider mocks={[getUser]} addTypename={false}>
                <App />
            </MockedProvider>
        );

        await wait(0); // wait for response

        const tree = component.toJSON();
        expect(tree.children).toEqual(['error...']);
    });
});
