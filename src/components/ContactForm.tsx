import React, { SyntheticEvent } from 'react';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { IContactInputs } from '../models/contact.model';
import { styles } from '../styles/Styles';

interface IProps {
    handleSubmit(event: SyntheticEvent): void;
    inputs: IContactInputs;
    handleInputChange(event: React.ChangeEvent<HTMLInputElement>): void;
    isEditMode: boolean;
    nameErrorMessage: string;
    emailErrorMessage: string;
}

const ContactForm: React.FunctionComponent<IProps> = ({
    handleSubmit,
    inputs,
    handleInputChange,
    isEditMode,
    nameErrorMessage,
    emailErrorMessage,
}) => {
    return (
        <div style={styles.header}>
            <form style={styles.form} onSubmit={handleSubmit}>
                <TextField
                    style={styles.inputTextField}
                    error={!!nameErrorMessage}
                    helperText={nameErrorMessage}
                    label="Name"
                    value={inputs.name}
                    name="name"
                    onChange={handleInputChange}
                />
                <TextField
                    style={styles.inputTextField}
                    error={!!emailErrorMessage}
                    helperText={emailErrorMessage}
                    label="Email"
                    value={inputs.email}
                    name="email"
                    onChange={handleInputChange}
                />
                <Button
                    style={styles.submitButton}
                    type="submit"
                    variant="contained"
                    color={isEditMode ? 'secondary' : 'primary'}
                    disabled={!!nameErrorMessage || !!emailErrorMessage}
                >
                    {isEditMode ? <>Edit</> : <>Create</>}
                </Button>
            </form>
        </div>
    );
};

export default ContactForm;
