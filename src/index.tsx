import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Routes from './components/Routes';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';

const client = new ApolloClient({ uri: 'http://localhost:3001' });

ReactDOM.render(
    <BrowserRouter>
        <ApolloProvider client={client}>
            <Routes />
        </ApolloProvider>
    </BrowserRouter>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
