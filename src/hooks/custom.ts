import { useState } from 'react';
import { ContactErrorMessage } from '../models/contact.model';

const useErrorHandling = (): [
    { [key: string]: string },
    (errorType: ContactErrorMessage, errorMsg: string) => void,
    () => void
] => {
    const [nameErrorMessage, setNameErrorMessage] = useState('');
    const [emailErrorMessage, setEmailErrorMessage] = useState('');

    const handleErrorMessage = (errorType: ContactErrorMessage, errorMsg: string) => {
        switch (errorType) {
            case ContactErrorMessage.Name:
                setNameErrorMessage(errorMsg);
                break;
            case ContactErrorMessage.Email:
                setEmailErrorMessage(errorMsg);
                break;
            default:
                clearErrorMessages();
                break;
        }
    };

    const clearErrorMessages = () => {
        setNameErrorMessage('');
        setEmailErrorMessage('');
    };

    return [{ nameErrorMessage, emailErrorMessage }, handleErrorMessage, clearErrorMessages];
};

export { useErrorHandling };
