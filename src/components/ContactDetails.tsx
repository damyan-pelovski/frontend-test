import React from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import { VIEW_CONTACT } from '../queries/contacts';

import { styles } from '../styles/Styles';
import { ArrowBack as ArrowBackIcon } from '@material-ui/icons';
import { dateFormatter } from '../utils/utils';
import { List, ListItem, ListItemIcon, ListItemText, Tooltip, IconButton, Card, CardContent } from '@material-ui/core';
import {
    Star as StarIcon,
    Person as PersonIcon,
    Email as EmailIcon,
    Today as CreatedAtIcon,
    Update as LastModifiedAtIcon,
} from '@material-ui/icons';

interface RouteParams {
    id: string;
}

const ContactDetails: React.FunctionComponent = () => {
    const params = useParams<RouteParams>();
    const router = useHistory();

    const { loading, error, data } = useQuery(VIEW_CONTACT, { variables: { id: params.id } });

    const navigateBack = () => {
        router.goBack();
    };

    if (loading) return <div>loading...</div>;
    if (error) return <div>error...</div>;

    const { contact } = data;

    const formattedCreatedAtDate = dateFormatter(contact.createdAt);
    const formattedModifiedAtDate = dateFormatter(contact.lastModifiedAt);

    return (
        <Card style={styles.card}>
            <Tooltip title="Go back">
                <IconButton onClick={navigateBack}>
                    <ArrowBackIcon />
                </IconButton>
            </Tooltip>
            <CardContent>
                <List aria-label="contact">
                    <ListItem>
                        <ListItemIcon>
                            <StarIcon />
                        </ListItemIcon>
                        <ListItemText primary={contact.id} />
                    </ListItem>
                    <ListItem>
                        <ListItemIcon>
                            <PersonIcon />
                        </ListItemIcon>
                        <ListItemText primary={contact.name} />
                    </ListItem>
                    <ListItem>
                        <ListItemIcon>
                            <EmailIcon />
                        </ListItemIcon>
                        <ListItemText primary={contact.email} />
                    </ListItem>
                    <ListItem>
                        <ListItemIcon>
                            <CreatedAtIcon />
                        </ListItemIcon>
                        <ListItemText primary={formattedCreatedAtDate} />
                    </ListItem>
                    <ListItem>
                        <ListItemIcon>
                            <LastModifiedAtIcon />
                        </ListItemIcon>
                        <ListItemText primary={formattedModifiedAtDate} />
                    </ListItem>
                </List>
            </CardContent>
        </Card>
    );
};

export default ContactDetails;
