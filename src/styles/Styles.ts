export const styles: { [key: string]: { [key: string]: string | number } } = {
    main: {
        width: '100%',
        maxWidth: '600px',
        margin: '20px auto',
    },
    heading: {
        fontSize: '32px',
    },
    header: {
        justifyContent: 'center',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
    },
    card: {
        padding: '20px',
        margin: '20px 0',
    },
    contact: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        wordBreak: 'break-word',
    },
    submitButton: {
        marginTop: '20px',
    },
    inputTextField: {
        marginBottom: '8px',
    },
};
