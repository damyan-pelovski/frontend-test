import React, { useState, SyntheticEvent } from 'react';
import { useQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import { Machine } from 'xstate';
import { useMachine } from '@xstate/react';

import { GET_CONTACTS, ADD_CONTACT, DELETE_CONTACT, EDIT_CONTACT } from './queries/contacts';
import ContactForm from './components/ContactForm';
import ContactsList from './components/ContactsList';

import { useErrorHandling } from './hooks/custom';
import { ContactErrorMessage, IContactInputs } from './models/contact.model';
import { styles } from './styles/Styles';

enum EditModeState {
    Active = 'active',
    Inactive = 'inactive',
}

//very simple example usage of finite state machine
const toggleEditModeMachine = Machine({
    initial: EditModeState.Inactive,
    states: {
        inactive: { on: { TOGGLE: EditModeState.Active } },
        active: { on: { TOGGLE: EditModeState.Inactive } },
    },
});

const defaultContactInput: IContactInputs = {
    id: '',
    name: '',
    email: '',
};

const App: React.FunctionComponent = () => {
    const client = useApolloClient();
    const { loading, error, ...contactsState } = useQuery(GET_CONTACTS);
    const [{ value: editModeCurrentState }, send] = useMachine(toggleEditModeMachine);
    const isEditModeActive = editModeCurrentState === EditModeState.Active;

    const [{ nameErrorMessage, emailErrorMessage }, handleErrorMessage, clearErrorMessages] = useErrorHandling();

    const [inputs, setInputs] = useState<IContactInputs>({ ...defaultContactInput });

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.persist();
        clearErrorMessages();
        setInputs(inputs => ({ ...inputs, [event.target.name]: event.target.value }));
    };

    const [deleteContact] = useMutation(DELETE_CONTACT, {
        onCompleted() {
            //option 1
            //force refetch on onComplete handler to get the latest contacts
            contactsState.refetch();
        },
    });

    const [addContact] = useMutation(ADD_CONTACT, {
        onCompleted({ addContact }) {
            //option 2
            //manually update the store and not force a refetch
            client.writeQuery({
                query: GET_CONTACTS,
                data: {
                    contacts: [addContact, ...contactsState.data.contacts],
                },
            });
        },
    });

    const [editContact] = useMutation(EDIT_CONTACT);

    const isFormValid = (inputs: IContactInputs) => {
        if (inputs.name.length === 0) {
            handleErrorMessage(ContactErrorMessage.Name, 'Name should be at least 1 character long.');
            return false;
        }

        if (!inputs.email) {
            handleErrorMessage(ContactErrorMessage.Email, 'Email is required.');
            return false;
        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(inputs.email)) {
            handleErrorMessage(ContactErrorMessage.Email, 'Please enter a valid email address.');
            return false;
        }
        return true;
    };

    const handleSubmit = (e: SyntheticEvent) => {
        e.preventDefault();

        const { id, name, email } = inputs;

        if (!isFormValid(inputs)) return;

        isEditModeActive
            ? editContact({
                  variables: { id, name, email, lastModifiedAt: new Date().toISOString() },
              })
                  .then(handleSucess)
                  .catch(handleError)
            : addContact({ variables: { name, email } })
                  .then(handleSucess)
                  .catch(handleError);

        //in any case we return to the create functionality
        isEditModeActive && send('TOGGLE');
    };

    const handleSucess = () => {
        setInputs({ ...defaultContactInput });
        clearErrorMessages();
    };

    const handleError = () => {
        //show the user a snackbar or anything to indicate an error
    };

    const removeContact = (id: string) => {
        deleteContact({ variables: { id } })
            .then(() => {
                //in any case we return to the create functionality
                isEditModeActive && send('TOGGLE');
                setInputs({ ...defaultContactInput });
                clearErrorMessages();
            })
            .catch(handleError);
    };

    const openEditForm = (id: string, name: string, email: string) => {
        !isEditModeActive && send('TOGGLE');
        setInputs({ id, name, email });
        clearErrorMessages();
    };

    if (loading) return <div>loading...</div>;
    if (error) return <div>error...</div>;

    const { contacts } = contactsState.data;

    return (
        <>
            <h1 style={styles.heading}>Contact Manager</h1>

            <h2>{isEditModeActive ? <>Edit Contact</> : <>Create Contact </>}</h2>
            <ContactForm
                handleSubmit={handleSubmit}
                inputs={inputs}
                nameErrorMessage={nameErrorMessage}
                emailErrorMessage={emailErrorMessage}
                handleInputChange={handleInputChange}
                isEditMode={isEditModeActive}
            />

            {contacts.length > 0 && (
                <ContactsList contacts={contacts} removeContact={removeContact} editContact={openEditForm} />
            )}
        </>
    );
};

export default App;
