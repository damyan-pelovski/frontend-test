import React from 'react';
import { Route, Switch } from 'react-router-dom';
import App from '../App';
import ContactDetails from './ContactDetails';
import ErrorPage from './ErrorPage';
import { Container } from '@material-ui/core';
import { styles } from '../styles/Styles';

const Routes: React.FunctionComponent = () => (
    <Container style={styles.main} maxWidth="md">
        <Switch>
            <Route path="/" component={App} exact />
            <Route path="/contact/:id" component={ContactDetails} exact />
            <Route component={ErrorPage} />
        </Switch>
    </Container>
);

export default Routes;
