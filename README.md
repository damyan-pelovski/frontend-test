# Contact Manager

## Available Scripts

### `npm run dev`

Runs both FE and graphql server in parallel.

### `npm run test`

Uses jest to run tests

## Features to implement

-   List Contacts
-   View Contact
-   Add Contact
-   Delete Contact
-   Edit Contact
-   Clean routing i.e '/contact/:id'
-   Use material-ui for components

### Bonuses

-   Use [xstate](https://xstate.js.org/docs) to manage app state.
-   Connect to the graphql endpoint http://localhost:3001 by using create-react-app proxy feature
-   Use the graphql endpoint to get/create/update/delete

### Contact

-   Name eg 'John Smith'
-   Email eg 'john@smith.com'
-   Date Modified eg '31-01-2018 15:04'
-   Date Created eg '31-01-2018 15:04'
