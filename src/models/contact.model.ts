export interface IContact {
    id: string;
    name: string;
    email: string;
    createdAt?: string;
    lastModifiedAt?: string;
}

export interface IContactInputs {
    id?: string;
    name: string;
    email: string;
}

export enum ContactErrorMessage {
    Name = 'Name',
    Email = 'Email',
}
