import React from 'react';
import { IContact } from '../models/contact.model';
import { useHistory } from 'react-router-dom';
import { Card, Tooltip, FormGroup, IconButton, CardContent } from '@material-ui/core';
import { Delete as DeleteIcon, Visibility as VisibilityIcon, Edit as EditIcon } from '@material-ui/icons';
import { styles } from '../styles/Styles';

interface IProps {
    contacts: IContact[];
    removeContact(id: string): void;
    editContact(id: string, name: string, email: string): void;
}

const ContactsList: React.FunctionComponent<IProps> = ({ contacts, removeContact, editContact }) => {
    const router = useHistory();

    const navigateContactDetails = (conactId: string) => {
        router.push(`/contact/${conactId}`);
    };

    return (
        <Card style={styles.card}>
            <CardContent>
                <h2>Contacts</h2>
                <FormGroup>
                    {contacts.map((contact, index) => (
                        <div key={index} style={styles.contact}>
                            {contact.name} - {contact.email}
                            <div style={styles.contact}>
                                <Tooltip title="Delete Contact">
                                    <IconButton onClick={() => removeContact(contact.id)}>
                                        <DeleteIcon />
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title="View Contact">
                                    <IconButton onClick={() => navigateContactDetails(contact.id)}>
                                        <VisibilityIcon />
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title="Edit Contact">
                                    <IconButton
                                        onClick={() => {
                                            editContact(contact.id, contact.name, contact.email);
                                        }}
                                    >
                                        <EditIcon />
                                    </IconButton>
                                </Tooltip>
                            </div>
                        </div>
                    ))}
                </FormGroup>
            </CardContent>
        </Card>
    );
};

export default ContactsList;
