import { gql } from 'apollo-boost';

const GET_CONTACTS = gql`
    query {
        contacts {
            name
            email
            id
        }
    }
`;

const VIEW_CONTACT = gql`
    query($id: ID) {
        contact(id: $id) {
            id
            name
            email
            createdAt
            lastModifiedAt
        }
    }
`;

const ADD_CONTACT = gql`
    mutation($name: String, $email: String) {
        addContact(contact: { name: $name, email: $email }) {
            id
            name
            email
        }
    }
`;

const EDIT_CONTACT = gql`
    mutation($id: ID, $name: String, $email: String, $lastModifiedAt: String) {
        updateContact(contact: { id: $id, name: $name, email: $email, lastModifiedAt: $lastModifiedAt }) {
            id
            name
            email
            lastModifiedAt
        }
    }
`;

const DELETE_CONTACT = gql`
    mutation($id: ID) {
        deleteContact(id: $id)
    }
`;

export { GET_CONTACTS, VIEW_CONTACT, ADD_CONTACT, DELETE_CONTACT, EDIT_CONTACT };
